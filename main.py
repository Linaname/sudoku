input_data = ['89*1***7*',
              '527*4*8*1',
              '16*7*8***',
              '6**473*2*',
              '249*8*7*3',
              '738*****5',
              '38269***7',
              '916**7284',
              '475812396']


class Table:
    digits = set(list('123456789'))
    rows_sets = [set() for _ in range(9)]
    columns_sets = [set() for _ in range(9)]
    squares_sets = [set() for _ in range(9)]
    data = [[None for _ in range(9)] for _ in range(9)]

    def __init__(self, input_data):
        init_data = [list(row) for row in input_data]
        for x in range(9):
            for y in range(9):
                cell = (x, y)
                value = init_data[x][y]
                if value not in self.digits:
                    continue
                self.add_value(cell, value)

    def row_set(self, cell):
        x, y = cell
        row_number = x
        return self.rows_sets[row_number]

    @staticmethod
    def row_neighbors(cell):
        x, y = cell
        for i in range(9):
            yield (x, i)

    def column_set(self, cell):
        x, y = cell
        column_number = y
        return self.columns_sets[column_number]

    @staticmethod
    def column_neighbors(cell):
        x, y = cell
        for i in range(9):
            yield (i, y)

    def square_set(self, cell):
        x, y = cell
        square_number = (x//3)*3 + y//3
        return self.squares_sets[square_number]

    @staticmethod
    def square_neighbors(cell):
        x, y = cell
        for i in range(x - x % 3, x+3 - x % 3):
            for j in range(y - y % 3, y+3 - y % 3):
                yield (i, j)

    def neighbors(self, cell):
        for neighbor in self.row_neighbors(cell):
            yield neighbor
        for neighbor in self.column_neighbors(cell):
            yield neighbor
        for neighbor in self.square_neighbors(cell):
            yield neighbor

    def add_value(self, cell, value):
        x, y = cell
        self.data[x][y] = value
        self.row_set(cell).add(value)
        self.column_set(cell).add(value)
        self.square_set(cell).add(value)
        for neighbor in self.neighbors(cell):
            neighbor_values = self.possible_values(neighbor)
            nx, ny = neighbor
            if len(neighbor_values) == 1 and self.data[nx][ny] not in self.digits :
                value = list(neighbor_values)[0]
                self.add_value(neighbor, value)

    def possible_values(self, cell):
        return self.digits \
               - self.row_set(cell) \
               - self.square_set(cell) \
               - self.column_set(cell)

    def show_data(self):
        for row in self.data:
            print(*row, sep=' ')


table = Table(input_data)
table.show_data()
